 /*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.common.connection;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class ContextDataResourceNames {

    public final static String MYSQL_GESTION_JDBC = "jdbc/gestion";
}
