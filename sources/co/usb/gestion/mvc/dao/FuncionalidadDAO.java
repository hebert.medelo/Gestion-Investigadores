/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.FuncionalidadDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class FuncionalidadDAO {

    /**
     *
     * @param conexion
     * @param idMenu
     * @param idUsuario
     * @return
     */
    public ArrayList<FuncionalidadDTO> listarFuncionalidadesPorMenu(Connection conexion, String idMenu, String idUsuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FuncionalidadDTO> listado = null;
        FuncionalidadDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT func.func_id, func.func_pagina, func.func_titulo ");
            cadSQL.append(" FROM funcionalidad AS func ");
            cadSQL.append(" INNER JOIN tipousuario_funcionalidad tifu ON tifu.func_id = func.func_id");
            cadSQL.append(" WHERE menu_id = ? AND  tifu.tius_id = ? ");
            cadSQL.append(" ORDER BY tifu.func_id asc ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idMenu, ps);
            AsignaAtributoStatement.setString(2, idUsuario, ps);
            rs = ps.executeQuery();
            listado = new ArrayList<FuncionalidadDTO>();

            while (rs.next()) {
                datos = new FuncionalidadDTO();
                datos.setId(rs.getString("func_id"));
                datos.setPagina(rs.getString("func_pagina"));
                datos.setTitulo(rs.getString("func_titulo"));

                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
}
