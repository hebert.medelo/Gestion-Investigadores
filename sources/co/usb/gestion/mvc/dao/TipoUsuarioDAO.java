/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.constantes.Constantes;
import co.usb.gestion.mvc.dto.TipoUsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class TipoUsuarioDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        TipoUsuarioDTO datosTipoUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tius_id, tius_descripcion, tius_estado FROM tipo_usuario WHERE tius_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datosTipoUsuario = new TipoUsuarioDTO();
                datosTipoUsuario.setId(rs.getString("tius_id"));
                datosTipoUsuario.setDescripcion(rs.getString("tius_descripcion"));
                datosTipoUsuario.setEstado(rs.getString("tius_estado"));

                listado.add(datosTipoUsuario);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarTipoUsuario(Connection conexion, TipoUsuarioDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO tipo_usuario (tius_descripcion, tius_estado) ");
            cadSQL.append(" VALUES (?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarTipoUsuario(Connection conexion, TipoUsuarioDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE tipo_usuario SET tius_descripcion = ?, tius_estado = ? WHERE tius_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(3, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }
}
