/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ClaseProductoDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String descripcion = Generales.EMPTYSTRING;
    String codigo = Generales.EMPTYSTRING;
    String idSubTipoProducto = Generales.EMPTYSTRING;
    String subtipoProducto = Generales.EMPTYSTRING;
    String idTipoProducto = Generales.EMPTYSTRING;
    String tipoProducto = Generales.EMPTYSTRING;
    String cclase = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;

    public String getCclase() {
        return cclase;
    }

    public void setCclase(String cclase) {
        this.cclase = cclase;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getIdSubTipoProducto() {
        return idSubTipoProducto;
    }

    public void setIdSubTipoProducto(String idSubTipoProducto) {
        this.idSubTipoProducto = idSubTipoProducto;
    }

    public String getSubtipoProducto() {
        return subtipoProducto;
    }

    public void setSubtipoProducto(String subtipoProducto) {
        this.subtipoProducto = subtipoProducto;
    }

    public String getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(String idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
