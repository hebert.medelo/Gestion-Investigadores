<%-- 
    Document   : edit-Producto
    Created on : 1/07/2018, 04:23:55 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Perfil Producto</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Operacional</a></li> 
    <li class="active"><strong>Editar Perfil Producto</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a id="cargar" data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div id="alert"></div>
                <div class="table-responsive" hidden id="divTabla">
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Subtipo</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody id="lisproductos"></tbody>
                        <tfoot>
                            <tr>
                                <th>Fecha</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Subtipo</th>
                                <th>Editar</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="button" class="btn btn-white btn-rounded" onclick="volver();">volver</button>                       
                        </div>
                    </div>
                </div>
                <form role="form" id="formEditar" action="return:false" autocomplete="off" hidden>
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required maxlength="50" title="Ingrese nombre">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="descripcion">Descripción</label>
                            <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="descripción" required maxlength="50" title="Ingrese la descripción">
                        </div>                     
                    </div> 
                    <div class="row">  
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="subtipo">Subtipo de producto</label>
                            <select onchange="javascript:listarClaseProductoSub(this.value,'claseProducto');" class="form-control" id="subtipo" name="subtipo" title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="claseProducto">Clase de producto</label>
                            <select class="form-control" id="claseProducto" name="claseProducto" title="Seleccione una opcion"></select>
                        </div>       
                    </div> 
                    <div class="row">                                                                
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="fecha">Fecha</label>
                            <input type="text" class="form-control" id="fecha" name="fecha" placeholder="Fecha" required maxlength="50" title="Ingrese fecha">
                        </div>                                                                 
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="url">URL producto</label>
                            <!--<input type="text" onclick="location.href = this.value" class="form-control" id="url" name="url" placeholder="URL del producto" required maxlength="255" title="Ingrese URL del producto">-->
                            <input type="text" onclick="window.open(this.value)" class="form-control" id="url" name="url" placeholder="URL del producto" required maxlength="255" title="Ingrese URL del producto">
                            
                        </div>                 
                    </div>  
                    <div class="row">                                                 
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="perfil">Investigador</label>
                            <select class="form-control" id="perfil" name="perfil" required title="Seleccione una opcion"></select>
                        </div>                 
                    </div>                        
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded" onclick="volver2()">Volver</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formEditar', 2);">Editar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                var operacion = null;
                                var idclase = null;
                                var IdPerPro = null;
                                $(document).ready(function () {
                                    consultarSubtipo();
                                    $('#cargar').click(function () {
                                        consultarSubtipo();
                                        return false;
                                    });
                                    listarSubtipoProducto("subtipo");
                                    listarPerfilInvestigador("perfil");
                                });
                                function volver() {
                                    $(".table-responsive").hide();
                                    $("#formEditar").show();
                                }
                                function volver2() {
                                    $("#formEditar").hide();
                                    $("#divTabla").show();
                                }
                                function actualizarPerfilProducto() {

                                    var Datos = {
                                        idPerfilProducto: IdPerPro,
                                        fecha: $("#fecha").val(),
                                        descripcion: $("#descripcion").val(),
                                        idSubtipoProducto: $("#subtipo").val(),
                                        nombre: $("#nombre").val(),
                                        claseproducto: $("#claseProducto").val(),
                                        url: $("#url").val(),
                                        idPerfil: $("#perfil").val()  
                                    };
                                    ajaxGestion.actualizarPerfilProducto(Datos, {
                                        callback: function (data) {
                                            if (data) {
                                                notificacion("alert-success", "Se <strong>actualizaron</strong> los datos on exíto.");
                                                //$("#formBuscar")[0].reset();
                                                volver2();
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>actualizar</strong> los datos.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

                                function ejecutarPostValidate() {
                                    //if (operacion == 1)
                                    //  consultarSubtipo();
                                    if (operacion == 2)
                                        actualizarPerfilProducto();
                                    // if (operacion == 2)
                                    operacion = null;
                                    IdFormacion = null;
                                }
                                function consultarSubtipo() {
                                    ajaxGestion.consultarSubtipoOperacional("", {
                                        callback: function (data) {
                                            if (data !== null) {
                                                listado = data;
                                                $('#myTable').dataTable().fnDestroy();
                                                dwr.util.removeAllRows("lisproductos");
                                                dwr.util.addRows("lisproductos", listado, mapa, {
                                                    escapeHtml: false
                                                });

                                                $(".table-responsive").show();
                                                $("#formEditar").hide();
                                                $('.dataTables-example').DataTable({
                                                    dom: '<"html5buttons" B>lTfgitp',
                                                    buttons: [
                                                        {
                                                            extend: 'copyHtml5',
                                                            exportOptions: {
                                                                columns: [0, ':visible']
                                                            }
                                                        },
                                                        {
                                                            extend: 'excelHtml5',
                                                            exportOptions: {
                                                                columns: ':visible'
                                                            }
                                                        },
                                                        {
                                                            extend: 'pdfHtml5',
                                                            exportOptions: {
                                                                columns: [0, 1, 2, 3, 4]
                                                            }
                                                        },
                                                        'colvis'
                                                    ]
                                                });
                                            } else {
                                                notificacion("alert-danger", "No se <strong>encontraron</strong> resultados.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                var listado = [];
                                var mapa = [
                                    function (data) {
                                        return data.fecha;
                                    },
                                    function (data) {
                                        return data.nombre;
                                    },
                                    function (data) {
                                        return data.descripcion;
                                    },
                                    function (data) {
                                        return data.descripcionSubtipoProd;
                                    },
                                    function (data) {
                                        return "<button class='btn btn-primary btn-rounded btn-xs' type='button'  onclick='verPerfilProducto(" + data.idPerfilProducto + ");'>Ver</button>";
                                    }
                                ];
                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }
                                function verPerfilProducto(id) {                                    
                                    for (var i = 0; i < listado.length; i++) {
                                        if (listado[i].idPerfilProducto == id) {
                                            $("#fecha").val(listado[i].fecha);
                                            $("#nombre").val(listado[i].nombre);
                                            /*$('#subtipo option')
                                             .filter(function () {
                                             return $.trim($(this).text()) == listado[i].supr;
                                             })
                                             .attr('selected', true);*/
                                            $("#subtipo").val(listado[i].idSubtipoProducto).trigger("change");
                                            //$("#claseProducto").val(listado[i].claseproducto).trigger("change");
                                            idclase = listado[i].claseproducto;

                                            $("#descripcion").val(listado[i].descripcion);
                                            $("#url").val(listado[i].url);
                                            $('#perfil option')
                                                    .filter(function () {
                                                        return $.trim($(this).val()) == listado[i].idPerfil;
                                                    })
                                                    .attr('selected', true);

                                            IdPerPro = listado[i].idPerfilProducto;
                                        }
                                    }



                                    $("#divTabla").hide();
                                    $("#formEditar").show();
                                }
                                function listarSubtipoProducto(idCombo, valorSeleccionado) {
                                    ajaxGestion.listarSubtipoProducto({
                                        callback: function (data) {
                                            dwr.util.removeAllOptions(idCombo);
                                            dwr.util.addOptions(idCombo, [{
                                                    id: '',
                                                    descripcion: 'Seleccione una opción'
                                                }], 'id', 'descripcion');
                                            if (data !== null) {
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                                $("#" + idCombo).val(valorSeleccionado).trigger("change");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                
                                function listarClaseProductoSub(idTipoP, idCombo,valorSeleccionado) {
                                    ajaxGestion.listarClaseProductoSub(idTipoP, {
                                        callback: function (data) {
                                            dwr.util.removeAllOptions(idCombo);
                                            dwr.util.addOptions(idCombo, [{
                                                    cclase: 'Seleccione una opción'
                                                }], 'cclase', 'cclase');
                                            
                                            if (data !== null) {
                                                dwr.util.addOptions(idCombo, data, 'cclase', 'cclase');
                                                if (idclase != null) {
                                                    jQuery("#" + idCombo).val(idclase);
                                                }
                                            }
                                            idclase = null;
                                        },
                                        timeout: 20000
                                    });
                                }
                                              
                                function listarPerfilInvestigador(idCombo) {
                                    ajaxGestion.listarPerfilInvestigador({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        nombre: 'Seleccione una opción'
                                                    }], 'id', 'nombre');
                                                dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                                                //jQuery("#" + idCombo).val(valorSeleccionado);
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                /*function listarSubtipoProducto(idCombo) {
                                    ajaxGestion.listarSubtipoProducto({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        descripcion: 'Seleccione una opción'
                                                    }], 'id', 'descripcion');
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }*/

</script>