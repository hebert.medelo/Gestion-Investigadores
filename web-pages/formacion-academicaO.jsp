<%-- 
    Document   : formacion-academica
    Created on : 1/07/2018, 04:23:55 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Formación Academica</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Operacional</a></li> 
    <li class="active"><strong>Formación Academica</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="fecha">Fecha de inicio</label>
                            <input type="text" class="form-control" id="fecha" name="fecha" placeholder="Año de inicio" required maxlength="50" title="Ingrese año de inicio">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="graduacion">Año de graduación</label>
                            <input type="text" class="form-control" id="graduacion" name="graduacion" placeholder="Año de graduación" required maxlength="50" title="Ingrese año de graduación">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nivel">Nivel de formación</label>
                            <select class="form-control" id="nivel" name="nivel" required title="Seleccione una opcion"></select>
                        </div>                                                 
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="perfil">Investigador</label>
                            <select class="form-control" id="perfil" name="perfil" required title="Seleccione una opcion"></select>
                        </div>                   
                    </div>
                    <div class="row"> 
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="institucion">Institución</label>
                            <input type="text" class="form-control" id="institucion" name="institucion" placeholder="Institución" required maxlength="100" title="Ingrese la Institución">
                        </div>   
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="programa">Programa academico</label>
                            <input type="text" class="form-control" id="programa" name="programa" placeholder="Programa academico" required maxlength="100" title="Ingrese el programa academico">
                        </div>                  
                    </div> 
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded">Cancelar</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar',2);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                var operacion = null;
                                $(document).ready(function () {
                                    listarNivelFormacion("nivel");
                                    listarPerfilInvestigador("perfil");
                                });
                                function volver() {
                                    $(".table-responsive").hide();
                                    $("#formRegistrar").show();
                                }
                                function registrarInfAcademica() {
                                    
                                    var Datos = {
                                        fecha: $("#fecha").val(),
                                        graduacion: $("#graduacion").val(),
                                        nivel: $("#nivel").val(),
                                        institucion: $("#institucion").val(),
                                        programa: $("#programa").val(),
                                        perfil: $("#perfil").val()                                        
                                        
                                    };
                                    
                                    ajaxGestion.registrarInfAcademica(Datos, {
                                        callback: function (data) {
                                            if (data) {
                                                notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                                                $("#formRegistrar")[0].reset();
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                               
                                function ejecutarPostValidate() {
                                    if (operacion == 2)
                                        registrarInfAcademica();
                                    // if (operacion == 2)
                                    operacion = null;
                                }
                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }                                
                                function listarNivelFormacion(idCombo) {
                                    ajaxGestion.listarNivelFormacion({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        descripcion: 'Seleccione una opción'
                                                    }], 'id', 'descripcion');
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                                //jQuery("#" + idCombo).val(valorSeleccionado);
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }                         
                                function listarPerfilInvestigador(idCombo) {
                                    ajaxGestion.listarPerfilInvestigador({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        nombre: 'Seleccione una opción'
                                                    }], 'id', 'nombre');
                                                dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                                                //jQuery("#" + idCombo).val(valorSeleccionado);
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

</script>