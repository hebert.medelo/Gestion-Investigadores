<%-- 
    Document   : index_ctr
    Created on : 27/10/2016, 04:08:38 PM
    Author     : Ing Hebetrt Medelo
--%>

<%@page import="co.usb.gestion.mvc.constantes.Usuarios"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="co.usb.gestion.mvc.dto.*"%>
<%@ page import="co.usb.gestion.common.util.*"%>

<jsp:useBean type="co.usb.gestion.mvc.fachada.FachadaSeguridad" scope="application" id="fachadaSeguridad"/>

<%
    PerfilDTO datosUsuario = null;

    try {

        if (!ValidaString.isNullOrEmptyString(request.getRemoteUser())) {

            datosUsuario = fachadaSeguridad.consultarDatosUsuarioLogueado(request.getRemoteUser());

            System.out.println("/////////////////// " + datosUsuario.toStringJson());

            if (datosUsuario != null && datosUsuario.getEstado().equals(Usuarios.ESTADO_ACTIVO)) {

                session.setAttribute("datosUsuario", datosUsuario);

                request.getRequestDispatcher("/index.jsp").forward(request, response);

            } else {
                request.getRequestDispatcher("/login.jsp?error=Ingreso no autorizado").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    } catch (Exception e) {
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }
%>   