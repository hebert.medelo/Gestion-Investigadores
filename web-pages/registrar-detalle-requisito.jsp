<%-- 
    Document   : registrar-detalle-requisito
    Created on : 24/06/2018, 06:01:08 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<h1 class="page-title">Registrar Detalle Requisito</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="form-basic.html">Detalle Requisito</a></li> 
    <li class="active"><strong>Registro</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Registro</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="tRequisito">Tipo Requisito</label>
                            <select onchange="verDiv();" class="form-control" id="tRequisito" name="tRequisito" required title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="opcion">Opción</label>
                            <select class="form-control" id="opcion" name="opcion" required title="Seleccione una opcion"></select>
                        </div> 
                    </div>
                    <div class="row">  
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="tInvestigador">Tipo de investigador</label>
                            <select onchange="javascript:listarSubTipoInvestigador(this.value, 'sInvestigador');" class="form-control" id="tInvestigador" name="tInvestigador" title="Seleccione una opcion" required></select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="sInvestigador">Subtipo Investigador</label>
                            <select onchange="" class="form-control" id="sInvestigador" name="sInvestigador" required title="Seleccione una opcion"></select>
                        </div>                       
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="tValida">Tipo validar</label>
                            <select onchange="verDiv(this.value);" class="form-control" id="tValida" name="tValida" required title="Seleccione una opcion"></select>
                        </div>                                               
                    </div> 
                    <div class="line-dashed" id="lineR1" hidden></div>
                    <div class="row" hidden id="divR1">                                            
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="Nformacion">Nivel de formaciòn</label>
                            <select class="form-control" id="Nformacion" name="Nformacion" title="Seleccione una opcion" required></select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="r1Cantidad">Cantidad</label>
                            <input type="text" class="form-control" id="r1Cantidad" name="r1Cantidad" value="1" disabled placeholder="Cantidad" required maxlength="1" title="Ingrese cantidad">
                        </div>
                    </div> 
                    <div class="row" hidden id="divR2">                                           
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="sProducto">Tipo Producto</label>
                            <select class="form-control" id="sProducto" name="sProducto" title="Seleccione una opcion" required></select>
                        </div> 
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="clase">Clase</label>
                            <select class="form-control" id="clase" name="clase" title="Seleccione una opcion" required>
                                <option value="">Selecione una opción</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="T">T</option>
                            </select> 
                        </div>
                    </div> 
                    <div class="row" hidden id="divR22">  
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="r2Cantidad">Cantidad</label>
                            <input type="tel" class="form-control" id="r2Cantidad" name="r2Cantidad" placeholder="Cantidad" required maxlength="2" title="Ingrese cantidad">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="anio">Año</label>
                            <input type="tel" class="form-control" id="anio" name="anio" placeholder="Año" required minlength="1" maxlength="2" title="Ingrese año">
                        </div>                     
                    </div> 
                    <div class="row" hidden id="divR3"> 
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="r3Producto">Tipo Producto</label>
                            <select onchange="listarSubtipoProductoPorIdProducto('Producto', this.value);" required class="form-control" id="r3Producto" name="r3Producto" title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="Producto">Subtipo Producto</label>
                            <select class="form-control" id="Producto" name="Producto" title="Seleccione una opcion" required ><option value="">Seleccione una opción</option></select>
                        </div> 
                    </div> 
                    <div class="row" hidden id="divR33">  
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="r3Cantidad">Cantidad</label>
                            <input type="tel" class="form-control" id="r3Cantidad" name="r3Cantidad" placeholder="Cantidad" required maxlength="2" title="Ingrese cantidad">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="r3anio">Año</label>
                            <input type="tel" class="form-control" id="r3anio" name="r3anio" placeholder="Año" required minlength="1" maxlength="2" title="Ingrese año">
                        </div>                     
                    </div> 
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded">Cancelar</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar', 1);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script>
    var operacion = null;
    $(document).ready(function () {
        listarTipoValida("tValida");
        listarNivelFormacion("Nformacion");
        listarTipoProducto("sProducto");
        listarTipoProducto("r3Producto");
        listarTipoRequisito("tRequisito");
        listarOpcionRequisito("opcion");
        listarSubTipoInvestigador("sInvestigador");
        listarTipoInvestigador("tInvestigador");
    });

    $("input[type=tel]").on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    function listarTipoInvestigador(idCombo, valorSeleccionado) {
        ajaxGestion.listarTipoInvestigador({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $("#" + idCombo).val(valorSeleccionado).trigger("change");
                }
            },
            timeout: 20000
        });
    }

    function registrarDetalleRequisito() {

        var dato1 = {
            idTipoRequisito: $("#tRequisito").val(),
            idOpcion: $("#opcion").val(),
            idsubTipoInvestigador: $("#tInvestigador").val(),
        };

        var r1 = $("#tValida").val();
        var r2 = $("#tRequisito").val();

        if (r1 == "1") {
            var datos2 = {
                idTipoValida: $("#tValida").val(),
                idNivelFormacion: $("#Nformacion").val(),
                cantidad: $("#r1Cantidad").val(),
            };
        }
        if (r1 == "2" && r2 != "3") {
            var datos2 = {
                idTipoValida: $("#tValida").val(),
                idTipoProducto: $("#sProducto").val(),
                clase: $("#clase").val(),
                cantidad: $("#r2Cantidad").val(),
                tiempo: $("#anio").val(),
            };

        }
        if (r1 == "2" && r2 == "3") {
            var datos2 = {
                idTipoValida: $("#tValida").val(),
                idTipoProducto: $("#sProducto").val(),
                idSubtipoProducto: $("#Producto").val(),
                cantidad: $("#r3Cantidad").val(),
                tiempo: $("#r3anio").val(),
            };
        }
       
        ajaxGestion.registrarDetalleRequisito(dato1, datos2, {
            callback: function (data) {
                console.log(data);
                if (data) {
                    notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                    $("#formRegistrar")[0].reset();
                } else {
                    notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                }
            },
            timeout: 20000
        });
    }

    function notificacion(t, m) {
        if ($('#alert').text() == "") {
            setTimeout('$("#alert").text("")', 3000);
        }
        $("#alert").text("");
        $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                + '<span aria-hidden="true">×</span>'
                + '</button>'
                + m
                + '</div>');
        $("#alert").focus();
    }

    function listarSubtipoProductoPorIdProducto(idCombo, id) {
        ajaxGestion.listarSubtipoProductoPorIdProducto(id, {
            callback: function (data) {
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        descripcion: 'Seleccione una opción'
                    }], 'id', 'descripcion');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function ejecutarPostValidate() {
        if (operacion == 1)
            registrarDetalleRequisito();
        // if (operacion == 2)
        operacion = null;
    }

    function listarSubTipoInvestigador(idTipoInv, idCombo, valorSeleccionado) {
        ajaxGestion.listarSubTipoInvestigador(idTipoInv, {
            callback: function (data) {
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        descripcion: 'Seleccione una opción'
                    }], 'id', 'descripcion');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarOpcionRequisito(idCombo) {
        ajaxGestion.listarOpcionRequisito({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoRequisito(idCombo) {
        ajaxGestion.listarTipoRequisito({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoValida(idCombo) {
        ajaxGestion.listarTipoValida({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarClaseProducto(idCombo) {
        ajaxGestion.listarClaseProducto({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoProducto(idCombo, valorSeleccionado) {
        ajaxGestion.listarTipoProducto({
            callback: function (data) {
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        descripcion: 'Seleccione una opción'
                    }], 'id', 'descripcion');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarNivelFormacion(idCombo, valorSeleccionado) {
        ajaxGestion.listarNivelFormacion({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $("#" + idCombo).val(valorSeleccionado).trigger("change");
                }
            },
            timeout: 20000
        });
    }

    function verDiv() {

        $("#divR1").hide();
        $("#divR2").hide();
        $("#divR22").hide();
        $("#divR3").hide();
        $("#divR33").hide();
        $(".form-group").removeClass("has-error");
        $('.error').remove();
        var r1 = $("#tValida").val();
        var r2 = $("#tRequisito").val();
        if (r1 == "1") {
            $("#lineR1").show();
            $("#divR1").show();
        }
        if (r1 == "2" && r2 != "3") {
            $("#lineR1").show();
            $("#divR2").show();
            $("#divR22").show();
        }
        if (r1 == "2" && r2 == "3") {
            $("#lineR1").show();
            $("#divR3").show();
            $("#divR33").show();
        }
    }

</script>